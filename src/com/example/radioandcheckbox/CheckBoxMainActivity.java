package com.example.radioandcheckbox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CheckBoxMainActivity extends Activity {

	Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkbox);
        
        btn_submit=(Button)findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent checkboxing=new Intent(CheckBoxMainActivity.this,CheckBoxHomePage.class);
				startActivity(checkboxing);
				finish();
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			}
		});
    
    }

}
