package com.example.radioandcheckbox;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;


public class MainActivity extends Activity {

	Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        btn_submit=(Button)findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent radio=new Intent(MainActivity.this,HomePage.class);
				startActivity(radio);
				finish();
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			}
		});
    
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_online:
                if (checked)
                    // Pirates are the best
                break;
            case R.id.radio_offline:
                if (checked)
                    // Ninjas rule
                break;
            case R.id.radio_direct:
                if (checked)
                    // Ninjas rule
                break;
        }
    }
    }
